FROM alpine:3.14.0
RUN apk add --update nginx

COPY hello.txt /var/www

CMD ["nginx", "-g", "daemon off;"]